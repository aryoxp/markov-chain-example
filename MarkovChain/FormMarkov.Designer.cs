namespace MarkovChain
{
    partial class FormMarkov
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Label label1;
            System.Windows.Forms.Label label2;
            System.Windows.Forms.Label label6;
            System.Windows.Forms.Label label3;
            System.Windows.Forms.Label label;
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.udColumn = new System.Windows.Forms.NumericUpDown();
            this.udRow = new System.Windows.Forms.NumericUpDown();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btClear = new System.Windows.Forms.Button();
            this.tbOutput = new System.Windows.Forms.RichTextBox();
            this.gbMarkov = new System.Windows.Forms.GroupBox();
            this.udIteration = new System.Windows.Forms.NumericUpDown();
            this.btProses = new System.Windows.Forms.Button();
            this.dgMarkov = new System.Windows.Forms.DataGridView();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.btResetAll = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.dgResult = new System.Windows.Forms.DataGridView();
            label1 = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            label6 = new System.Windows.Forms.Label();
            label3 = new System.Windows.Forms.Label();
            label = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udColumn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.udRow)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.gbMarkov.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udIteration)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgMarkov)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgResult)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(16, 23);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(33, 13);
            label1.TabIndex = 1;
            label1.Text = "Rows";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new System.Drawing.Point(123, 23);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(47, 13);
            label2.TabIndex = 3;
            label2.Text = "Columns";
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.Location = new System.Drawing.Point(245, 179);
            label6.Name = "label6";
            label6.Size = new System.Drawing.Size(31, 13);
            label6.TabIndex = 11;
            label6.Text = "; x =";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label3.Location = new System.Drawing.Point(229, 175);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(21, 13);
            label3.TabIndex = 10;
            label3.Text = "(x)";
            // 
            // label
            // 
            label.AutoSize = true;
            label.Location = new System.Drawing.Point(221, 179);
            label.Name = "label";
            label.Size = new System.Drawing.Size(13, 13);
            label.TabIndex = 12;
            label.Text = "P";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(label2);
            this.groupBox1.Controls.Add(this.udColumn);
            this.groupBox1.Controls.Add(label1);
            this.groupBox1.Controls.Add(this.udRow);
            this.groupBox1.Location = new System.Drawing.Point(12, 67);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(405, 52);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Markov Chain Matrix Parameter";
            // 
            // udColumn
            // 
            this.udColumn.Location = new System.Drawing.Point(176, 21);
            this.udColumn.Name = "udColumn";
            this.udColumn.Size = new System.Drawing.Size(46, 21);
            this.udColumn.TabIndex = 2;
            this.udColumn.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.udColumn.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.udColumn.ValueChanged += new System.EventHandler(this.udColumn_ValueChanged);
            // 
            // udRow
            // 
            this.udRow.Location = new System.Drawing.Point(55, 21);
            this.udRow.Name = "udRow";
            this.udRow.Size = new System.Drawing.Size(49, 21);
            this.udRow.TabIndex = 0;
            this.udRow.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.udRow.ValueChanged += new System.EventHandler(this.udRow_ValueChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.btClear);
            this.groupBox2.Controls.Add(this.tbOutput);
            this.groupBox2.Location = new System.Drawing.Point(423, 67);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(262, 450);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Debug Message";
            // 
            // btClear
            // 
            this.btClear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btClear.Location = new System.Drawing.Point(181, 421);
            this.btClear.Name = "btClear";
            this.btClear.Size = new System.Drawing.Size(75, 23);
            this.btClear.TabIndex = 1;
            this.btClear.Text = "Clear";
            this.btClear.UseVisualStyleBackColor = true;
            // 
            // tbOutput
            // 
            this.tbOutput.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tbOutput.Location = new System.Drawing.Point(6, 19);
            this.tbOutput.Name = "tbOutput";
            this.tbOutput.Size = new System.Drawing.Size(250, 396);
            this.tbOutput.TabIndex = 0;
            this.tbOutput.Text = "";
            this.tbOutput.WordWrap = false;
            // 
            // gbMarkov
            // 
            this.gbMarkov.Controls.Add(label6);
            this.gbMarkov.Controls.Add(label3);
            this.gbMarkov.Controls.Add(this.udIteration);
            this.gbMarkov.Controls.Add(this.btProses);
            this.gbMarkov.Controls.Add(this.dgMarkov);
            this.gbMarkov.Controls.Add(label);
            this.gbMarkov.Location = new System.Drawing.Point(12, 125);
            this.gbMarkov.Name = "gbMarkov";
            this.gbMarkov.Size = new System.Drawing.Size(405, 205);
            this.gbMarkov.TabIndex = 2;
            this.gbMarkov.TabStop = false;
            this.gbMarkov.Text = "Markov Chain (One Step) Transition Matrix";
            // 
            // udIteration
            // 
            this.udIteration.Location = new System.Drawing.Point(276, 177);
            this.udIteration.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.udIteration.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.udIteration.Name = "udIteration";
            this.udIteration.Size = new System.Drawing.Size(42, 21);
            this.udIteration.TabIndex = 9;
            this.udIteration.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.udIteration.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // btProses
            // 
            this.btProses.Location = new System.Drawing.Point(324, 176);
            this.btProses.Name = "btProses";
            this.btProses.Size = new System.Drawing.Size(75, 23);
            this.btProses.TabIndex = 1;
            this.btProses.Text = "Proses";
            this.btProses.UseVisualStyleBackColor = true;
            this.btProses.Click += new System.EventHandler(this.btProses_Click);
            // 
            // dgMarkov
            // 
            this.dgMarkov.AllowUserToAddRows = false;
            this.dgMarkov.AllowUserToDeleteRows = false;
            this.dgMarkov.AllowUserToResizeColumns = false;
            this.dgMarkov.AllowUserToResizeRows = false;
            this.dgMarkov.BackgroundColor = System.Drawing.Color.White;
            this.dgMarkov.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgMarkov.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgMarkov.ColumnHeadersVisible = false;
            this.dgMarkov.Location = new System.Drawing.Point(6, 20);
            this.dgMarkov.Name = "dgMarkov";
            this.dgMarkov.RowHeadersVisible = false;
            this.dgMarkov.RowHeadersWidth = 10;
            this.dgMarkov.Size = new System.Drawing.Size(393, 150);
            this.dgMarkov.TabIndex = 0;
            this.dgMarkov.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgMarkov_CellValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(12, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(175, 29);
            this.label4.TabIndex = 3;
            this.label4.Text = "Markov Chain";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(17, 42);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(171, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Aryo Pinandito / NRP.9109205506";
            // 
            // btResetAll
            // 
            this.btResetAll.Location = new System.Drawing.Point(610, 25);
            this.btResetAll.Name = "btResetAll";
            this.btResetAll.Size = new System.Drawing.Size(75, 23);
            this.btResetAll.TabIndex = 5;
            this.btResetAll.Text = "Reset All";
            this.btResetAll.UseVisualStyleBackColor = true;
            this.btResetAll.Click += new System.EventHandler(this.btResetAll_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.dgResult);
            this.groupBox3.Location = new System.Drawing.Point(12, 336);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(405, 181);
            this.groupBox3.TabIndex = 6;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Markov Chain Resulting Transition Matrix";
            // 
            // dgResult
            // 
            this.dgResult.AllowUserToAddRows = false;
            this.dgResult.AllowUserToDeleteRows = false;
            this.dgResult.AllowUserToResizeColumns = false;
            this.dgResult.AllowUserToResizeRows = false;
            this.dgResult.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgResult.BackgroundColor = System.Drawing.Color.White;
            this.dgResult.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgResult.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgResult.ColumnHeadersVisible = false;
            this.dgResult.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgResult.Location = new System.Drawing.Point(6, 20);
            this.dgResult.Name = "dgResult";
            this.dgResult.RowHeadersVisible = false;
            this.dgResult.RowHeadersWidth = 10;
            this.dgResult.Size = new System.Drawing.Size(393, 155);
            this.dgResult.TabIndex = 0;
            // 
            // FormMarkov
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(697, 529);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.btResetAll);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.gbMarkov);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "FormMarkov";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Markov Chain";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udColumn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.udRow)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.gbMarkov.ResumeLayout(false);
            this.gbMarkov.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.udIteration)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgMarkov)).EndInit();
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgResult)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.NumericUpDown udRow;
        private System.Windows.Forms.NumericUpDown udColumn;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btClear;
        private System.Windows.Forms.RichTextBox tbOutput;
        private System.Windows.Forms.GroupBox gbMarkov;
        private System.Windows.Forms.DataGridView dgMarkov;
        private System.Windows.Forms.Button btProses;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btResetAll;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.DataGridView dgResult;
        private System.Windows.Forms.NumericUpDown udIteration;
    }
}

