using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace MarkovChain
{
    public partial class FormMarkov : Form
    {
        Matrix markov;

        public FormMarkov()
        {
            InitializeComponent();
            InitialValue();
        }

        #region Initial Value
        /// <summary>
        /// Method untuk memberikan nilai awal pada matrix rantai
        /// probabilitas transisi markov yang pertama P(x);x=1
        /// </summmary>
        private void InitialValue()
        {
            tbOutput.Clear();

            udColumn.Value = 4;
            udRow.Value = 4;
            udIteration.Value = 2;

            markov = new Matrix((Int32)udRow.Value, (Int32)udColumn.Value);
            List<Double[]> row = new List<Double[]>();
            row.Add(new Double[] { 0.080, 0.184, 0.368, 0.368 });
            row.Add(new Double[] { 0.632, 0.368, 0.000, 0.000 });
            row.Add(new Double[] { 0.264, 0.368, 0.368, 0.000 });
            row.Add(new Double[] { 0.080, 0.184, 0.368,	0.368 });

            // Fill matrix with default data
            for (Int32 i = 0; i< row.Count; i++)
            {
                for (Int32 j = 0; j < row[i].Length; j++)
                {
                    markov[i, j] = row[i][j];
                }
            }

            // Tampilkan matrix
            ShowMatrix(markov);
            dgResult.DataSource = null;
        }
        #endregion

        #region Debug Function
        /// <summary>
        /// Method untuk menampilkan Matrix m pada Debug Message
        /// </summary>
        /// <param name="m">Matrix yang ditampilkan</param>
        /// <param name="name">Label (nama) matrix yang ditampilkan</param>
        public void Debug(Matrix m, String name)
        {
            tbOutput.SelectionFont = new Font("Tahoma", (float)8, FontStyle.Bold);
            tbOutput.SelectedText += " " + name + "\r\n";
            tbOutput.SelectionFont = new Font("Tahoma", (float)8, FontStyle.Regular);
            tbOutput.SelectedText += m.ToString() + "\r\n\r\n";

        }
        #endregion

        #region Display Matrix
        /// <summary>
        /// Method untuk menampilkan matrix m pada DataGridView
        /// </summary>
        /// <param name="m">Matrix yang ditampilkan</param>
        private void ShowMatrix(Matrix m)
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();

            // Menyusun kolom matrix m untuk dimasukkan ke DataSet
            for (Int32 i = 0; i < udColumn.Value; i++)
            {
                DataColumn dc = new DataColumn(i.ToString());
                dt.Columns.Add(dc);
            }

            // Mengisi baris-baris matrix m untuk dimasukkan ke DataSet
            for (Int32 i = 0; i < m.NoRows; i++)
            {
                List<Object> col = new List<Object>();
                for (Int32 j = 0; j < m.NoCols; j++)
                {
                    Double val = m[i, j];
                    col.Add(val.ToString());
                }
                dt.Rows.Add(col.ToArray());
            }

            // Tampilkan matrix m pada DataGridView
            ds.Tables.Add(dt);
            dgMarkov.DataSource = ds.Tables[0];

            for (Int32 i = 0; i < dgMarkov.Columns.Count; i++) dgMarkov.Columns[i].Width = 50;

        }
        #endregion

        #region Display Result Matrix
        /// <summary>
        /// Method untuk menampilkan Matrix probabilitas
        /// transisi dari rantai markov
        /// </summary>
        /// <param name="m">Matrix yang hendak ditampilkan</param>
        private void ShowResultMatrix(Matrix m)
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();

            // menyusun kolom matrix m pada DataSet
            for (Int32 i = 0; i < udColumn.Value; i++)
            {
                DataColumn dc = new DataColumn(i.ToString());
                dt.Columns.Add(dc);
            }

            // megisi data baris-baris matrix DataSet
            for (Int32 i = 0; i < m.NoRows; i++)
            {
                List<Object> col = new List<Object>();
                for (Int32 j = 0; j < m.NoCols; j++)
                {
                    Double val = m[i, j];
                    col.Add(String.Format("{0:0.#######}", val));
                }
                dt.Rows.Add(col.ToArray());
            }

            // Tampilkan matrix pada DataSetView
            ds.Tables.Add(dt);
            dgResult.DataSource = ds.Tables[0];

            for (Int32 i = 0; i < dgResult.Columns.Count; i++) dgResult.Columns[i].Width = 50;

        }
        #endregion

        #region Proses Rantai Markov
        /// <summary>
        /// Proses Rantai Markov pada saat tombol proses diklik
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btProses_Click(object sender, EventArgs e)
        {
            tbOutput.Clear();

            // Jumlah iterasi yang diinginkan
            Int32 iteration = (Int32)udIteration.Value;

            // Simpan nilai awal matrix probabilitas transisi rantai markov
            Matrix P = markov;
            try
            {
                // Lakukan kalkulasi sebanyak jumlah iterasi
                
                for (Int32 i = 1; i < iteration; i++)
                {
                    // Perkalian matrix probabilitas 
                    // Simpan hasilnya pada matrix hasil P 
                    P = Matrix.Multiply(P, markov);
                    // Tampilkan hasilnya
                    Debug(P, "Matrix P(" + (i + 1) + "):");
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information); }

            // Tampilkan hasil akhir dari iterasi
            ShowResultMatrix(P);
        }
        #endregion

        #region Mengubah dimensi baris dan kolom pada matrix data
        /// <summary>
        /// Event ketika nilai komponen updownNumeric baris diubah
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void udRow_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                // Variabel Matrix sementara untuk menampung nilai matrix yang baru
                Matrix temp;
                // Jika baris matrix diperbesar, copy nilainya dari matrix markov
                if (udRow.Value > markov.NoRows)
                {
                    temp = new Matrix(markov.NoRows + 1, markov.NoCols);
                    for (Int32 i = 0; i < markov.NoRows; i++)
                    {
                        for (Int32 j = 0; j < markov.NoCols; j++)
                        {
                            temp[i, j] = markov[i, j];
                        }
                    }
                }
                // Jika baris matrix diperkecil, copy nilainya dari matrix markov
                else
                {
                    temp = new Matrix(markov.NoRows - 1, markov.NoCols);
                    for (Int32 i = 0; i < temp.NoRows; i++)
                    {
                        for (Int32 j = 0; j < markov.NoCols; j++)
                        {
                            temp[i, j] = markov[i, j];
                        }
                    }
                }
                // Simpan hasilnya pada Matrix markov
                markov = temp;
                // Tampilkan hasilnya
                ShowMatrix(temp);
            }
            catch (Exception ex) {  };
        }

        /// <summary>
        /// Event ketika nilai komponen updownNumeric kolom diubah
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void udColumn_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                Matrix temp;
                // Jika kolom matrix diperbesar, copy nilainya dari matrix markov
                if (udColumn.Value > markov.NoCols)
                {
                    temp = new Matrix(markov.NoRows, markov.NoCols + 1);
                    for (Int32 i = 0; i < markov.NoRows; i++)
                    {
                        for (Int32 j = 0; j < markov.NoCols; j++)
                        {
                            temp[i, j] = markov[i, j];
                        }
                    }
                }
                // Jika kolom matrix diperkecil, copy nilainya dari matrix markov
                else
                {
                    temp = new Matrix(markov.NoRows, markov.NoCols - 1);
                    for (Int32 i = 0; i < markov.NoRows; i++)
                    {
                        for (Int32 j = 0; j < temp.NoCols; j++)
                        {
                            temp[i, j] = markov[i, j];
                        }
                    }
                }
                // Simpan hasilnya pada Matrix markov
                markov = temp;
                // Tampilkan hasilnya
                ShowMatrix(temp);
            }
            catch (Exception ex) { };
        }
        #endregion

        #region validasi perubahan nilai data pada matrix markov
        /// <summary>
        /// Event ketika nilai sebuah cell data matrix markov selesai diubah oleh user
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgMarkov_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                // Konversi nilai baru yang dimasukkan
                String cellValue = (String) dgMarkov.Rows[e.RowIndex].Cells[e.ColumnIndex].Value;
                Double NewValue = Double.Parse(cellValue);
                markov[e.RowIndex, e.ColumnIndex] = NewValue;
            }
            catch { }
            // Tampilkan matrix yang telah diubah
            ShowMatrix(markov);
        }
        #endregion

        #region Reset nilai data ke nilai default pada awal program
        /// <summary>
        /// Event ketika tombol Reset diklik
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btResetAll_Click(object sender, EventArgs e)
        {
            InitialValue();
        }
        #endregion


    }
}